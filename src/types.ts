import { ContextFrom, EventFrom } from 'xstate'
import { GameModel } from './front/machine/GameMachine'

export enum PlayerColor {
  RED = 'R',
  YELLOW= 'Y',
}

export type Position = {
  x: number
  y: number
}

export enum GameStates {
  LOBBY = 'LOBBY',
  PLAY = 'PLAY',
  VICTORY = 'VICTORY',
  DRAW = 'DRAW'
}

export type Player = {
  id: string,
  name: string,
  color?: PlayerColor
}

export type CellEmpty = 'E'
export type CellState = "R" | "Y" | CellEmpty | PlayerColor.RED | PlayerColor.YELLOW
export type GridState = CellState[][]

// use a helper to export the game model, allow to get an object that represent our context
export type GameContext = ContextFrom<typeof GameModel>
export type GameEvents = EventFrom<typeof GameModel>
export type GameEvent<T extends GameEvents["type"] = GameEvents["type"]> = GameEvents & {type: T}
export type GameGuard<T extends GameEvents["type"]> = (
  context: GameContext,
  event: GameEvent<T>
) => boolean // return true if we can do trnsition, it allow to get an auto-completion on this obj GameGuard
export type GameAction<T extends GameEvents["type"]> = (
  context: GameContext,
  event: GameEvent<T>
) => Partial<GameContext>
