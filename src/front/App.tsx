import { NameSelector } from "./screen/NameSelector";
import "./index.css";
import { ColorSelector } from "./screen/ColorSelector";
import { PlayerColor } from "../types";
import { Grid } from "./screen/Grid";

function App() {

  return (
    <div className="container">
      <Grid grid={[
          ["E", "E", "E", "E", "E", "E", "R"],
          ["E", "E", "E", "E", "E", "R", "Y"],
          ["E", "E", "E", "E", "E", "R", "R"],
          ["E", "E", "E", "E", "E", "R", "Y"],
          ["E", "E", "E", "E", "E", "Y", "R"],
          ["E", "E", "E", "E", "E", "Y", "Y"]
      ]} />
      <hr/>
      <NameSelector disabled onSelect={() => null} />
      <hr/>
      <ColorSelector onSelect={() => null} players={[{
        id: '1',
        name: 'hugues',
        color: PlayerColor.RED
      }, {
        id: '2',
        name: 'zoidberg',
        color: PlayerColor.YELLOW
      }]} colors={[PlayerColor.YELLOW, PlayerColor.RED]}
      />
    </div>
  )
}

export default App
