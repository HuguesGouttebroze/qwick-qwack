import { FormEvent, useState } from "react";

// on props level, we can add a type
type NameSelectorProps = {
    // the type is a "onSelect", this is a method that receve a string & qui ne retourne rien (void)
    onSelect: (name: string) => void,
    disabled: boolean
}

// after add the type, can say an objet of type "NameSelectorProps"
export function NameSelector ({onSelect, disabled}: NameSelectorProps) {
    const [error, setError] = useState('');
    // FormEvent is a React SynteticEvent
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault(); // don't want default comportement
        const name = new FormData(e.currentTarget as HTMLFormElement).get('name')
        if (!name || name.toString().trim() === '') {
            setError('non non non, renseigner preudo SVP')
            return;
        }
        onSelect(name.toString()); // use"toString"method to force the type 
    }

    return (
       <>
        <h3>Entrer votre pseudo</h3>
        {error && <div className="alert">
            {error}
            <button onClick={() => setError('')} className="alert__closed"> &times;</button>
        </div>}
        <form action="" onSubmit={handleSubmit}> 
            <label htmlFor="name">Pseudo</label>
            <input type="text" id="name" name="name" />
            <button disabled={disabled}>Choisir</button>
        </form>
       </> 
    )
}