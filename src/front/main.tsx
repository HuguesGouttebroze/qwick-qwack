import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { interpret } from 'xstate'
import { GameMachine, GameModel } from './machine/GameMachine'

const machine = interpret(GameMachine).start() // to create new machine instentiations
console.log(machine.send(GameModel.events.join('1', '1')).changed)
console.log(machine.send(GameModel.events.join('1', '1')).changed) // changed methode alow to test if state has changed


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)
