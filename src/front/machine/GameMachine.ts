import { createMachine } from "xstate";
import { createModel } from "xstate/lib/model";
import { GridState, Player, PlayerColor } from "../../types";
import { canJoinGuard } from "./guards";

// create "enum" types (rather than string)
enum GameStates {
    // define differents states
    LOBBY = "LOBBY", // player are waiting
    PLAY = "PLAY", // players are playing game
    VICTORY = "VICTORY", // win if 4 pings are alligns
    DRAW  = "DRAW", // if there's no winner when aff the grid is used 
}

export const GameModel = createModel({
    // define the model context
    players: [] as Player[],
    currentPlayer: null as null | Player['id'],
    rowLength: 4,
    grid: [
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E"],
    ] as GridState // take a look to the types.ts file that simplify this grid system & thes states comes from 
  }, {
    events: {
        join: (playerId: Player["id"], name: Player["name"]) => 
        ({playerId, name}),// allow to create an EVENT with a `GameModel.events.join()` and to know params and also to generate the events from the GameModel (see type file)
        leave: (playerId: Player["id"]) => ({playerId}),
        chooseColor: (playerId: Player["id"], color: PlayerColor) => 
        ({playerId, color}),
        start: (playerId: Player["id"]) => ({playerId}),  
        dropToken: (playerId: Player["id"], x: number) => ({playerId, x}),
        restart:  (playerId: Player["id"]) => ({playerId}), // to restart game
    }
});

export const GameMachine = GameModel.createMachine({
    // define transitions to move from states
    // init the state
    id: "game",
    context: GameModel.initialContext,
    initial: GameStates.LOBBY,
    states: { 
        // LOBBY's state transitions
        [GameStates.LOBBY]: {
            on: {
                join: { // reafect to a new partie, so it make come back to the state begin LOBBY, we don't move of our state
                    cond: canJoinGuard,
                    target: GameStates.LOBBY
                },
                chooseColor: { // stay to LOBBY state
                    target: GameStates.LOBBY
                },
                leave: {
                    target: GameStates.LOBBY
                },
                start: { // start & play directly
                    target: GameStates.LOBBY
                }
            } 
        },
        // PLAY's state transitions
        [GameStates.PLAY]: {
            on: {
                dropToken: { // put a ring in our grid
                    target: GameStates.VICTORY
                        // dropToken give to differents state, to implements later          
                }
            }
        },
        // VICTORY's state transitions
        [GameStates.VICTORY]: {
            on: {
                restart: { // in case of victory, redirect to the lobby state, or to play state also ?
                    target: GameStates.LOBBY 
                        // dropToken give to differents state, to implements later          
                }
            }
        },
        // DRAW's state transitions
        [GameStates.DRAW]: {
            on: {
                restart: { // in case of equality, I decide to redirect to the PLAY state
                    target: GameStates.PLAY 
                        // dropToken give to differents state, to implements later          
                }
            }
        },
    }
})
