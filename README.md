# Implémentation d'un jeu PUISSANCE 4

C'est parti pour programmer un jeu de Puissance 4 en `TypeScript`. J'utiliserais `React.js`, `node.js`, `Fastify`, `XState`, les `websockets` ... 

* un puissance 4 connecté où 2 joueurs pourront se défier ...

    * Le 1er user choisira un preudo, recuperera un URL à partager pr inviter les autres joueurs ... LE DEFI EST LANCE !!! incroyable ! lol !

    * le joueur 2 choisit ensuite un pseudo si il accepte le défi !!!

    * les joueurs choissent une partie

    * le créateur de la partie la lance

    * Sur un grille (Grid CSS) de 7 columns par 6 rows, les jours placent leurs pions.

    * Pour gagner la partie, un joueur doit aligner au moins 4 pions verticalement, horizontalement ou en diagonale.

## Dependances/technos
* `Node.js`
* `TypeScript` (débuter par l'écriture des type ds le fichier `types.ts`)
* composants Front-End sous `React`
* on utilisera les `Websocket`
* et le concept de State Machine avec le librairie `X State`
* `Fastify` pour le routage
## Liens et packages NPM 
- https://www.npmjs.com/package/reconnecting-websocket
- https://www.npmjs.com/package/@fastify/websocket

## Etapes

- Machine à état (tester tant que possible)
- Interface
- Jeu hors ligne
- Mise en place du serveur *
- Jeu en ligne *
- Mise en ligne (déploiement)

* okay, et ben c'est parti pour coder !

### XSTATE, machine à état
* voir concept & autre lib tel `robot`
* cmd s'installation avec `pnpm`
    `pnpm install xstate`
* State Machine : 
    - on définie un nouvel objet `createMachine()`
    - puis on y définie les états possible de notre jeu :
        - `Le lobby`, qui permettra aux joueurs de rejoindre la partie et de choisir sa couleur
        - `Le jeu`, où les joueurs pourront déposer à tour de rôle des pièces
        - `La victoire`
        - `L'égalité` si aucun des joueurs n'a réussi à aligner 4 pions

    - ds nos `state`, il faut définir les états `transition`

* Ensuite, on passe au modèle du jeu, nouvel objet `createMode()`
* ajout de conditions
    * ajout new files `guards.ts`, fonction pr emêcher une transition
    
* voici un des manières d'implémenter notre machine à état
    
    * code extrait du fichier `./src/front/machine/GameMachine.ts`

```js
import { createMachine } from 'xstate';

createMachine({
  id: 'game',
  context: "LOBBY",
  initial: "LOBBY",
  states: {
    LOBBY: {
      on: {
        join: {
          cond: "canJoin",
          actions: ["joinGame"],
          target: "LOBBY"
        },
        leave: {
          cond: "canLeave",
          actions: ["leaveGame"],
          target: "LOBBY"
        },
        chooseColor: {
          cond: "canChooseColor",
          target: "LOBBY",
          actions: ["chooseColor"],
        },
        start: {
          cond: "canStartGame",
          target: "PLAY",
          actions: ["setCurrentPlayer"]
        }
      }
    },
    PLAY: {
      after: {
        20000: {
          target: "PLAY",
          actions: ["switchPlayer"],
        }
      },
      on: {
        dropToken: [
          {
            cond: "isDrawMove",
            target: "DRAW",
            actions: ["dropToken"]
          },
          {
            cond: "isWiningMove",
            target: "VICTORY",
            actions: ["saveWiningPositionsActions", "dropToken"]
          },
          {
            cond: "canDrop",
            target: "PLAY",
            actions: ["dropToken", "switchPlayer"]
          }
        ]
      }
    },
    VICTORY: {
      on: {
        restart: {
          target: "LOBBY",
          actions: ["restart"]
        }
      }
    },
    DRAW: {
      on: {
        restart: {
          target: "LOBBY",
          actions: ["restart"]
        }
      }
    }
  }
})
```
* code extrait du fichier `./src/front/machine/actions.ts`

```ts
import { GameAction, GameContext, PlayerColor } from '../types'
import { currentPlayer, freePositionY, winingPositions } from '../func/game'
import { GameModel } from './GameMachine'

export const joinGameAction: GameAction<"join"> = (context, event) => ({
  players: [...context.players, {id: event.playerId, name: event.name}]
})

export const leaveGameAction: GameAction<"leave"> = (context, event) => ({
  players: context.players.filter(p => p.id !== event.playerId)
})

export const chooseColorAction: GameAction<"chooseColor"> = (context, event) => ({
  players: context.players.map(p => {
    if (p.id === event.playerId) {
      return {...p, color: event.color}
    }
    return p
  })
})

export const dropTokenAction: GameAction<"dropToken"> = ({grid, players}, {x: eventX, playerId}) => {
  const playerColor = players.find(p => playerId === p.id)!.color!
  const eventY = freePositionY(grid, eventX)
  const newGrid = grid.map((row, y) => row.map((v, x) => x === eventX && y === eventY ? playerColor : v))
  return {
    grid: newGrid
  }
}

export const switchPlayerAction = (context: GameContext) => ({
  currentPlayer: context.players.find(p => p.id !== context.currentPlayer)!.id
})

export const saveWiningPositionsActions: GameAction<"dropToken"> = (context, event) => ({
  winingPositions: winingPositions(
    context.grid,
    currentPlayer(context).color!,
    event.x,
    context.rowLength
  )
})

export const restartAction: GameAction<"restart"> = (context) => ({
  winingPositions: [],
  grid: GameModel.initialContext.grid,
  currentPlayer: null
})

export const setCurrentPlayerAction = (context: GameContext) => ({
  currentPlayer: context.players.find(p => p.color === PlayerColor.YELLOW)!.id
})
```
* et le code extrait du fichier `./src/front/machine/guards.ts

```ts
import { countEmptyCells, currentPlayer, freePositionY, winingPositions } from '../func/game'

export const canJoinGuard: GameGuard<"join"> = (context, event) => {
  return context.players.length < 2 && (context.players.find(p => p.id === event.playerId) === undefined)
}

export const canLeaveGuard: GameGuard<"leave"> = (context, event) => {
  return !!context.players.find(p => p.id === event.playerId)
}

export const canChooseColorGuard: GameGuard<"chooseColor"> = (context, event) => {
  return [PlayerColor.RED, PlayerColor.YELLOW].includes(event.color) &&
    context.players.find(p => p.id === event.playerId) !== undefined &&
    context.players.find(p => p.color === event.color) === undefined
}

export const canStartGameGuard: GameGuard<"start"> = (context, event) => {
  return context.players.filter(p => p.color).length === 2
}

export const canDropGuard: GameGuard<"dropToken"> = (context, event) => {
  return event.x < context.grid[0].length &&
    event.x >= 0 &&
    context.currentPlayer === event.playerId &&
    freePositionY(context.grid, event.x) >= 0
}

export const isWiningMoveGuard: GameGuard<"dropToken"> = (context, event) => {
  return canDropGuard(context, event) && winingPositions(
    context.grid,
    currentPlayer(context).color!,
    event.x,
    context.rowLength
    ).length > 0
}

export const isDrawMoveGuard: GameGuard<"dropToken"> = (context, event) => {
  return canDropGuard(context, event) && countEmptyCells(context.grid) <= 1
}
```
### React
* pour représenter une croix, au lieu de bfaire un x, il y a le `&times;`

### TypeScript Doc. MEMO
* quelques notes sur les différents types utilisés

* enum (ou Énumérations)
    *Les énumérations sont l'une des rares fonctionnalités de TypeScript qui ne sont pas une extension au niveau du type de JavaScript.

    *Les énumérations permettent à un développeur de définir un ensemble de constantes nommées. L'utilisation d'énumérations peut faciliter la documentation de l'intention ou créer un ensemble de cas distincts. TypeScript fournit des énumérations numériques et basées sur des chaînes.

* utilisations d'un helpers, un type utilitaire fournis par XState
    * utilisé ds le type file, on export notre context de jeu en tant qu'objet, on recupère à partir du `GameModel`
    * Permet de récupérer ts les types
    * Ainsi ns avon un objet `GameContext` qui correspond au context, 
    ce qui permet d'avoir les même types, tel que :

depuis le fichier `types.ts`
```ts
    type GameContext = {
        players: Player[];
        currentPlayer: string | null;
        rowLength: number;
        grid: GridState;
    }
```
* Type utilitaire eventsFrom
pr générer des évenemen à partie du modèle GameModel
ts les prochain events sont simplifé, les types sont créer automatiquement, juste en passant l'id, sans devoir passer le nom ...etc ...

* La partie INTERFACE

### Inspiration
* Retour aux années 80, quand j'étais encore tout môme, devans mon amiga 500, quel veinard! :-)

* Après on s'attaque à l'implémentation d'un `snake game`, en console, où nous utiliserons la programmation fonctionnelle, un programme se voulant le moins gourmand possible, pour tourner sur n'importe quelle machine. 

* Tt en repensant à mes interminable parties de `Marble Madness`, de `Rick Dangerous` ... etc ... sur mon amiga 500, lol !, j'ouvre `VIM` sur mon linux, et c'est partie pour coder la machine à état :-) 

## ! TO DO after coding & before deploy !

* remove all no util code comments
just keep comment juged util to understand better the content code elements (imagine discovering the code, must be simple & comments must help on the sens, no other!) 